﻿/// Arthur M. Thomé
/// 28 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class MouseLook : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private float m_mouseSensitivity = 100f;
    [ SerializeField ] private Transform m_playerBody = null;
    
    private float m_xRotation = 0f;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        Cursor.lockState = CursorLockMode.Locked;
    }
    
    void Update ( )
    {
        float mouseX = Input.GetAxis ( "Mouse X" ) * m_mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis ( "Mouse Y" ) * m_mouseSensitivity * Time.deltaTime;

        m_xRotation -= mouseY;
        m_xRotation = Mathf.Clamp ( m_xRotation, -90f, 90f );

        transform.localRotation = Quaternion.Euler ( m_xRotation, 0f, 0f );
        m_playerBody.Rotate ( Vector3.up * mouseX );
    }

    #endregion
}
