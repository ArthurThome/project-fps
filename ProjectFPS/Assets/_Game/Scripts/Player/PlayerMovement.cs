﻿/// Arthur M. Thomé
/// 28 MAY 2020

#region Statements

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#endregion

public class PlayerMovement : MonoBehaviour
{
    #region Fields

    [ SerializeField ] private CharacterController m_controller = null;

    [ SerializeField ] private float m_speed = 7f;

    private Vector3 m_velocity = Vector3.zero;
    private float m_gravity = -19.62f;

    private float m_jumpHeight = 3f;

    [ SerializeField ] private Transform m_groundCheck = null;
    [ SerializeField ] private float m_groundDistance = 0.4f;
    [ SerializeField ] private LayerMask m_groundMask;

    private bool m_inGrounded = false;

    #endregion

    #region MonoBehavior Methods

    void Start ( )
    {
        
    }
    
    void Update ( )
    {
        m_inGrounded = Physics.CheckSphere ( m_groundCheck.position, m_groundDistance, m_groundMask );

        if ( m_inGrounded && Input.GetButtonDown ( "Jump" ) )
        {
            m_velocity.y = Mathf.Sqrt ( m_jumpHeight * -2f * m_gravity );
        }

        if ( m_inGrounded && m_velocity.y < 0 )
        {
            m_velocity.y = -2f;
        }


        float x = Input.GetAxis ( "Horizontal" );
        float z = Input.GetAxis ( "Vertical" );

        Vector3 mov = transform.right * x + transform.forward * z;

        m_controller.Move ( mov * m_speed * Time.deltaTime );

        m_velocity.y += m_gravity * Time.deltaTime;

        m_controller.Move ( m_velocity * Time.deltaTime );
    }

    #endregion
}